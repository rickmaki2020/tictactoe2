#pragma once

#include <string>
#include <vector>
#include <iostream>

using namespace std;



class TicTacToe
{

private:

	char m_board[9] = {};
	int m_numsTurns = 0;
	char m_playerTurn = 'X';
	char m_winner = m_playerTurn ;

public:


	TicTacToe() { }


	char GetPlayerTurn()
	{
		if (m_numsTurns == 0)
		{
			return 'X';
		}
		else if (m_numsTurns == 1 )
		{
			return 'O';
		}
		if (m_numsTurns == 2)
		{
			return 'X';
		}
		else if (m_numsTurns == 3)
		{
			return 'O';
		}
		if (m_numsTurns == 4)
		{
			return 'X';
		}
		else if (m_numsTurns == 5)
		{
			return 'O';
		}
		if (m_numsTurns == 6)
		{
			return 'X';
		}
		else if (m_numsTurns == 7)
		{
			return 'O';
		}
		if (m_numsTurns == 8)
		{
			return 'X';
		}
	}
	

	bool IsValidMove(int position)
	{
		if (m_board[position] == 'X')
		{
			return false;
		}
		else if (m_board[position] == 'O')
		{
			return false;
		}
		else if ((position < 1) || (position > 9))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	bool IsOver()
	{
		if ((m_board[0] == 'O' && m_board[3] == 'O' && m_board[6] == 'O') || (m_board[0] == 'X' && m_board[3] == 'X' && m_board[6] == 'X'))
			return true;
		else if ((m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O') || (m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X'))
			return true;
		else if ((m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O') || (m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X'))
			return true;
		else if ((m_board[0] == 'O' && m_board[1] == 'O' && m_board[2] == 'O') || (m_board[0] == 'X' && m_board[1] == 'X' && m_board[2] == 'X'))
			return true;
		else if ((m_board[3] == 'O' && m_board[4] == 'O' && m_board[5] == 'O') || (m_board[3] == 'X' && m_board[4] == 'X' && m_board[5] == 'X'))
			return true;
		else if ((m_board[6] == 'O' && m_board[7] == 'O' && m_board[8] == 'O') || (m_board[6] == 'X' && m_board[7] == 'X' && m_board[8] == 'X'))
			return true;
		else if ((m_board[0] == 'O' && m_board[4] == 'O' && m_board[8] == 'O') || (m_board[0] == 'X' && m_board[4] == 'X' && m_board[8] == 'X'))
			return true;
		else if ((m_board[6] == 'O' && m_board[4] == 'O' && m_board[2] == 'O') || (m_board[6] == 'X' && m_board[4] == 'X' && m_board[2] == 'X'))
			return true;
		else return false;
	}
	void DisplayBoard() const
	{
		cout << m_board;
	}
	void DisplayResult()
	{
		if (m_numsTurns >= 9)
		{
			cout << "It's a tie!";
		}
		else
		{
			cout << m_winner << " is the winner!";
		}
	}
	void Move(int position)
	{
		if (m_playerTurn == 'X')
		{
			int n = position;
			m_board[n] = 'X';
			m_numsTurns++;
		}
		else
		{
			int n = position;
			m_board[n] = 'O';
			m_numsTurns++;
		}
	}
	
};

